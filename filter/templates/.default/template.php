<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<form>
    <div>
        Цвет<br/>
        <select name="filter[COLOR]">
            <option value="0">Любой</option>
            <? foreach ($arResult['COLORS'] as $el) { ?>
                <option value="<?=$el['ID']?>" <?if($arResult['REQUEST']['COLOR'] == $el['ID']) echo 'selected';?>><?=$el['NAME']?></option>
            <? } ?>
        </select>
    </div>

    <input type="text" name="filter[PRICE_FROM]" value="<?=$arResult['REQUEST']['PRICE_FROM']?>" placeholder="Цена от">

    <input type="submit" value="Показать" name="submit">
</form>