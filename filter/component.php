<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/* 
 *	Выборка данных для построения фильтра
 *	Пример с PHP кешированием. Это можно использовать когда не надо плодить версии кеша для разных комбинаций фильтра, не надо делать зависимые параметры.
 *	Если в кеше надо учитывать параметры фильтра - можно использовать обычный кеш компонента через $this->startResultCache()
 */
$cache = new CPHPCache();
$cacheTime = 86400;
$cacheId = 'custom_filter_form';
$cachePath = '/custom_filter_form/';

if ($cacheTime > 0 && $cache->InitCache($cacheTime, $cacheId, $cachePath)) {

    $res = $cache->GetVars();
    if (is_array($res['result'])){
        $arResult = $res['result'];
    }
}
else {

    CModule::IncludeModule('iblock');

    $res = CIBlockElement::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => COLORS_IBLOCK_ID, 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME'));
    while ($el = $res->Fetch()){
        $arResult['COLORS'][$el['ID']] = $el;
    }

    // тут все выборки для построения фильтра

    // можно использовать тегированный кеш чтобы кеш фильтра автоматически сбрасывался при каких-то событиях
    global $CACHE_MANAGER;
    $CACHE_MANAGER->StartTagCache($cachePath);
    $CACHE_MANAGER->RegisterTag('iblock_id_' . COLORS_IBLOCK_ID);
    $CACHE_MANAGER->EndTagCache();

    $cache->StartDataCache($cacheTime, $cacheId, $cachePath);
    $cache->EndDataCache(array("result" => $arResult));
}

/* 
 *	Обработка применения фильтра
 *	На основе данных из формы формируем массив фильтра.
 *	Далее массив фильтра передаем в параметр компонента news.list или catalog.section: 'FILTER_NAME' => 'filterCatalog'
 *  или используем массив при выборках через API
 */

if ($_REQUEST['filter']){

    foreach ($_REQUEST['filter'] as $field=>$val){
        $arResult['REQUEST'][$field] = htmlspecialchars($val);
    }

    global $filterCatalog;

    if ($arResult['REQUEST']['COLOR']){
        $filterCatalog['PROPERTY_COLOR'] = $arResult['REQUEST']['COLOR'];
    }

    if ($arResult['REQUEST']['PRICE_FROM']){
        $filterCatalog['>=PROPERTY_PRICE'] = $arResult['REQUEST']['PRICE_FROM'];
    }
}

$this->IncludeComponentTemplate();