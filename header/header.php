<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Page\Asset;

// если нужны языковые файлы
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<!DOCTYPE html>
<html>
<head lang="<?=LANGUAGE_ID?>">
    <title><?$APPLICATION->ShowTitle()?></title>

    <?
    $asset = Asset::getInstance();
    $asset->addCss(SITE_TEMPLATE_PATH . '/css/fonts.css');
    $asset->addCss(SITE_TEMPLATE_PATH . '/css/style.css');

    $asset->addJs(SITE_TEMPLATE_PATH . '/js/plugins/jquery/jquery-2.1.4.min.js');
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/main.js');

    $asset->addString("<link href='http://fonts.googleapis.com/css?family=PT+Sans:400&subset=cyrillic' rel='stylesheet' type='text/css'>"); 

    $APPLICATION->ShowHead();
    ?>
</head>
<body>
    <?$APPLICATION->ShowPanel();?>

    <h1><?$APPLICATION->ShowTitle(false)?></h1><?// обязательно с false?>

    <p><?=Loc::getMessage('header_MESSAGE')?></p><?// вывод фразы из языкового файла?>

    #WORK_AREA#

</body>
</html>