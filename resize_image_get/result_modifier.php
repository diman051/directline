<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

// пример для детальной страницы элемента (news.detail или catalog.element)
if ($arResult['DETAIL_PICTURE']['ID']){

    $file = CFile::ResizeImageGet(
        $arResult['DETAIL_PICTURE']['ID'], // в качестве исходной картинки можно использовать ID детального изображение товара, или ID картинки анонса $arResult['PREVIEW_PICTURE']['ID']
        array('width' => 100, 'height' => 100), // в какой размер нужно ресайзить
        1, // тип ресайза: 1 - с соблюдением пропорций, 2 - вырезать точный размер после ресайза
        true,
        array(),
        false,
        80 // качество сжатия в %. По дефолту в битриксе 95% на основе настроек модуля или инфоблока
    );

    // сокращенные записи
    $file = CFile::ResizeImageGet($arResult['DETAIL_PICTURE']['ID'], array('width' => 100, 'height' => 100), 1, true, array(), 80);
    $file = CFile::ResizeImageGet($arResult['DETAIL_PICTURE']['ID'], array('width' => 100, 'height' => 100), 1); // когда не надо менять степень сжатия

    // результат ресайза можно поместить в картинку анонса, чтобы не делать правки в самом шаблоне. Или можно сохранить в отдельном ключе $arResult
    $arResult['PREVIEW_PICTURE']['SRC'] = $file['src'];
   
    // если в шаблоне сайта используются параметры WIDTH и HEIGHT картинки, можно заменить эти параметры на основе результатов ресайза.
    $arResult['PREVIEW_PICTURE']['WIDTH'] = $file['width'];
    $arResult['PREVIEW_PICTURE']['HEIGHT'] = $file['height'];
}


// пример для списка элементов (news.list или catalog.section)
foreach ($arResult['ITEMS'] as $key=>$arItem){

    if ($arItem['DETAIL_PICTURE']['ID']){

        $file = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'], array('width' => 100, 'height' => 100), 1, true, array(), 80);
        $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] = $file['src'];
        $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['WIDTH'] = $file['width'];
        $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['HEIGHT'] = $file['height'];
    }
}
?>